// Chat-box
var chatHead = document.getElementsByClassName('chat-head');
var chatBody = document.getElementsByClassName('chat-body');
var chatText = document.getElementById('isi-chat');
var chatLocInsert = document.getElementsByClassName('msg-insert');
var Pengirim = true;

$(chatHead).click(function(){
    $(chatBody).toggle();
});

$(chatText).keypress(function(e){
    if (e.keyCode === 13) {
        if (Pengirim){
            $(chatLocInsert).append('<p class="msg-send">'+chatText.value+'</p>')
            Pengirim=false;
        } else {
            $(chatLocInsert).append('<p class="msg-receive">'+chatText.value+'</p>')
            Pengirim=true;
        }
        chatText.value='';
    }
});
// END